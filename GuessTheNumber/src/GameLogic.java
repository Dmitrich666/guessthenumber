import jdk.nashorn.internal.objects.annotations.Getter;

import javax.swing.*;

class GameLogic extends Phrases {

    private String playerName;
    private Integer chosenNumber;
    private Integer computerChoose;

    private String getPlayerName() {
        return playerName;
    }

    void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    private Integer getComputerChoose() {
        return computerChoose;
    }

    private void setComputerChoose(Integer computerChoose) {
        this.computerChoose = computerChoose;
    }

    private Integer getChosenNumber() {
        return chosenNumber;
    }

    private void setChosenNumber(Integer chosenNumber) {
        this.chosenNumber = chosenNumber;
    }

    void playerRegistration() {
        JOptionPane.showMessageDialog(null, "Привет! Я загадал число от 1 до 10!"
                + '\n' + "Угадаешь с трех раз?");
        try {
            String playerName = JOptionPane.showInputDialog("Как звать-то тебя?");
            playerName = playerName.trim();
            while (playerName == null || playerName.equals("")) {
                playerName = JOptionPane.showInputDialog("Однако, имя ввести придется");
            }
            setPlayerName(playerName);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Не хочешь - не надо.");
            System.exit(0);
        }
    }

    void playerChooseNumber() {
        try {
            String chooseYourNumber = JOptionPane.showInputDialog("Ну, вводи число, "
                    + getPlayerName() + '\n' +
                    "И не нажимай на кнопку отмены. А то прога упадет");

            if (chooseYourNumber.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Нужен ввод!");
                playerChooseNumber();
            } else if (chooseYourNumber.matches("[a-zA-Z]+") || chooseYourNumber.matches("[а-яА-Я]+")) {
                JOptionPane.showMessageDialog(null, "Сюда буквы вводить не надо");
                playerChooseNumber();
            } else {
                setChosenNumber(Integer.parseInt(chooseYourNumber));
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Это поле для цифр предназначено как бы...");
            playerChooseNumber();
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Просил же...");
            int i = JOptionPane.showConfirmDialog(null, "Давай еще раз? Только не хулигань",
                    "Повторим?", JOptionPane.YES_NO_OPTION);
            if (i == JOptionPane.YES_OPTION) {
                GameStart gameStart = new GameStart();
                gameStart.setPlayerName(getPlayerName());
                gameStart.computerChooseNumber();
                gameStart.playerChooseNumber();
                gameStart.numberCheck();
            } else {
                JOptionPane.showMessageDialog(null, "Ну и не надо... Счастливо оставаться!");
                System.exit(0);
            }
        }
    }

    void computerChooseNumber() {
        Integer computerChoose = (int) (Math.random() * 10 + 1);
        setComputerChoose(computerChoose);
        System.out.println(getComputerChoose());
    }

    void numberCheck() {

        int attemptCounter = 3;

        for (int i = 0; i < 3; i++) {

            if (!getChosenNumber().equals(getComputerChoose()) && attemptCounter > 1) {
                String failPhrase = printPhrase();
                attemptCounter--;
                JOptionPane.showMessageDialog(null, failPhrase +
                        '\n' + " Попыток осталось: " + attemptCounter);
                playerChooseNumber();
            } else if (getChosenNumber().equals(getComputerChoose())) {
                JOptionPane.showMessageDialog(null, "Очуметь! Как ты угадал?" +
                        '\n' + "Хватит играться!");
                System.exit(0);
            }
        }
        JOptionPane.showMessageDialog(null, "Не, " + playerName + ". Я загадал: "
                + getComputerChoose());
        int i = JOptionPane.showConfirmDialog(null, "Еще разок?", "Повторим?",
                JOptionPane.YES_NO_OPTION);
        if (i == JOptionPane.YES_OPTION) {
            GameStart gameStart = new GameStart();
            gameStart.setPlayerName(getPlayerName());
            gameStart.computerChooseNumber();
            gameStart.playerChooseNumber();
            gameStart.numberCheck();
        } else JOptionPane.showMessageDialog(null, "Ну и не надо... Пока...");
    }
}











