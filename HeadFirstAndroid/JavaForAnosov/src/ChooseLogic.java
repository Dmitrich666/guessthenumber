import java.util.ArrayList;
import java.util.Collections;

public class ChooseLogic {

    public ArrayList<Integer> choose() {
        ArrayList<Integer> playerTicket = new ArrayList<>();
        for (int i = 1; i <= 90; i++) {
            playerTicket.add(i);
        }
        Collections.shuffle(playerTicket);

        ArrayList <Integer> fifteen = new ArrayList<>();
        for(int i = 0; i < 15; i++){
            fifteen.add(playerTicket.get(i));
        }
        return fifteen;
    }
}